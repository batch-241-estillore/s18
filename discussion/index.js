function printName(name){
	console.log("My name is "+name);
}

printName("Juana");
printName("Gab");

function createFullName(firstName, middleName, lastName){
	console.log(firstName+" "+middleName+" "+lastName);
}

createFullName("Erven", "Joshua", "Cabral");

function checkDivisibilityBy8(num){
	let remainder = num%8;
	console.log("The remainder of "+num+" divided by 8 is "+remainder);
	let isDivisibleBy8 = (remainder === 0);
	console.log("Is "+num+" divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

/*
	Mini-Activity
		1. Create a function which is able to receive data as an argument(name of your favorite superhero)
			-display the name of fave suphero
*/
/*function displaySuperhero(nameSuper){
	nameSuper = prompt("Who is your fave superhero?");
	console.log("My favorite superhero is "+nameSuper);
}
displaySuperhero();*/

function returnFullName(firstName, middleName, lastName){
	return firstName+" "+middleName+" "+lastName;
	console.log("Can we print this message?");
}

let completeName = returnFullName("John","Doe","Smith");
console.log(completeName);
console.log(returnFullName("John","Doe","Smith"));
completeName = returnFullName("Jeru", "Nebur", "Palma");
console.log(completeName);

function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
}

let myAddress = returnAddress("Sta. mesa, Manila", "Philippines");
console.log(myAddress);


//Consider the ff code:
function printPlayerInformation(username, level, job){
	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);
	return ("Username: "+username+" \n"+"Level: "+level+" \n"+"Job: "+job);
}

let user1 = printPlayerInformation("cardo123","999","Immortal");
console.log(user1)
