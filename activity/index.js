console.log("Hello World");

function addTwoNum(num1, num2){
	let sum = num1 + num2;
	console.log("Displayed sum of "+num1+" and "+num2);
	console.log(sum);
}
addTwoNum(5,15);

function subtractTwoNum(num1, num2){
	let diff = num1 - num2;
	console.log("Displayed difference of "+num1+" and "+num2);
	console.log(diff);
}
subtractTwoNum(20,5);

function multiplyTwoNum(num1, num2) {
	return num1*num2;
};

function divideTwoNum(num1, num2) {
	return num1/num2;
};
let num3 = 61, num4 = 13;
let product = multiplyTwoNum(num3,num4);
let quotient = divideTwoNum(num3,num4);

console.log("The product of "+num3+" and "+num4);
console.log(product);
console.log("The quotient of "+num3+" and "+num4);
console.log(quotient);

function getAreaCircle(radius){
	return (radius**2)*3.1416;
}
let num5 = 41;
let circleArea = getAreaCircle(num5);
console.log("The result of getting the area of a circle with "+ num5 + " radius:");
console.log(circleArea);

function getAverage(num1, num2, num3, num4){
	return (num1+num2+num3+num4)/4;
}
let n1=5, n2=10, n3=80, n4=100;
let averageVar = getAverage(n1,n2,n3,n4);
console.log("The average of "+n1+","+n2+","+n3+","+n4 +" is:")
console.log(averageVar);

function getPercentage(num1, num2){
	let percentage = (num1/num2)*100;
	return percentage>=75
}
let n5=89, n6=150;
let isPassed = getPercentage(n5,n6);
console.log("Is "+n5+"/"+n6+" a passing score?");
console.log(isPassed);